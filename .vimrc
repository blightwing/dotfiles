syntax on

" Set number of spaces in tab
set tabstop=4 softtabstop=4
" Shiftwith = number of spaces used for each step of (auto)indent (local to
" buffer)
set shiftwidth=4
set exrc
set relativenumber
set nu 
set hidden 
set expandtab
set smartindent 
set noswapfile
set incsearch
set scrolloff=8
set undodir=~/.vim/undodir
set undofile


" vim-plug stuff
" https://github.com/junegunn/vim-plug
"
" - For Neovim: stdpath('data') . '/plugged'
" - Avoid using standard Vim directory names like 'plugin'
call plug#begin('~/.vim/plugged')

" Make sure you use single quotes
" Note ~ Any ~ /valid/ git URL is allowed
Plug 'https://github.com/junegunn/vim-github-dashboard.git'

Plug 'gruxbox-community/gruvbox'
Plug 'jremmen/vim-ripgrep'
Plug 'tpope/vim-fugitive'
Plug 'leafgarland/typescript-vim'
Plug 'vim-utils/vim-man'
Plug 'kien/ctrlp.vim.git'
Plug 'ycm-core/YouCompleteMe'
Plug 'mbbill/undotree'

call plug#end()

let mapleader = " "

" Allow RipGrep to find root 
if executable('rg')
    let g:rg_derive_root='true'
endif

let g:netrw_browse_split=2

" Custom Maps. Leader is <SPACE>, so commands are SPACE+letter
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR>
nnoremap <leader>pv :wincmd v<bar> :Ex <bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<SPACE>
nnoremap <silent> <leader>+ :vertical resize +5<CR>
nnoremap <silent> <leader>- :vertical resize -5<CR>

" YCM 'The Best Part'
nnoremap <silent> <leader>gd :YcmCompleter GoTo<CR>
" nnoremap <slent> <leader>gf :YcmCompleter FixIt<CR>


" Run gruvbox last
" autocmd vimenter * ++nested colorscheme gruvbox
